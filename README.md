# How to host static HTML on GitLab Pages:

Here is a list of resources that helped me get through.

## How to set up GitLab Pages:

* YouTube tutorial: https://www.youtube.com/watch?v=Cs6YxW9mr6Y

* Supplementary document: https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/

TLDR: make a index.html and .gitlab-ci.yml file in the root directory.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Test</title>
</head>
<body>
    <h1>Test</h1>
</body>
</html>
```

```yml
pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r * .public
  - mv .public public
  artifacts:
    paths:
    - public
  only:
  - main
```

## How to view page after publishing:

* See forum on how to find project URL: https://forum.gitlab.com/t/solved-find-the-gitlab-pages-url-on-the-ui/40261/3

TLDR: Deploy > Pages

## How to attach existing custom domain name:

* YouTube example: https://www.youtube.com/watch?v=dkDZi4x0UiE

TLDR: In "Deploy > Pages" add your domain name, then copy and paste the info from there to your domain control panel...

1. Create DNS "A" record "35.185.44.232".

2. Create DNS "CNAME" record "... ALIAS ...".

3. Create DNS "TXT" record "_gitlab-pages... TXT gitlab-pages...".

Check ownership of domain in GitLab by hitting refresh, make sure https is valid, and "Primary domain" is set to your domain only.

Wait for DNS propagation, should take MAX 15 minutes.

If works, you're done. 😊